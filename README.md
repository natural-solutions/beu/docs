
# BEU Docs

**FR**
Documentation pour les projets de la communauté BEU (Biodiversité et écosystèmes urbains).

**EN**
Documentation for the Biodiversity and Urban Ecosystems (BUE) community.


# Starting the project locally

## With Docker and Unix

    docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material


## Without Docker

    pip install mkdocs-material --user
    mkdocs serve
    

## Then point your browser to:
    localhost:8000

# Useful links

## [Material Docs](https://squidfunk.github.io/mkdocs-material/)
The theme for MKDocs we are using to build this documentation.

## [MkDocs](https://squidfunk.github.io/mkdocs-material/)
The original static site framework Material Docs is based on.

## [Markdown](https://daringfireball.net/projects/markdown/)
The text-to-HTML syntax which allows you to write and format texts very easily.
Check this [guide for Markdown](https://www.markdownguide.org).