Here is our workflow for Ecoteka and for most of Natural Solution's projects on Gitlab.

## 1/ Create an issue and a merge request

In the GitLab project, click on _issues>new issue_. Name the issue with the feature, fix you will work on. Example:

    Add subfooter

Assign the issue to yourself (and your coworker).

Click on the right label in the list (bug, story etc).

Click on _create the issue_.

Click on _create merge request_.

## 2/ Switch to this branch and write your code

In your local repository, pull the changes to retrieve the new branch:

    git pull

Switch to the new branch. Example:

    git checkout 18-add-subfooter

Code!

## 2 bis/ For the front-end, run a build

Check if the build works:

    npm run build

In Ecoteka:

    docker-compose exec frontend npm run build

## 3/ Commit and push your changes

    git add .
    git commit -m "feat: add subfooter"
    git push

## 4/ Mark the issue as ready

In GitLab, click on _mark as ready_ in the merge request tab.
You can compare the differences between your code and the original one in the _changes_ tab.

## 5/ Notify the other devs and ask for a review

In the Teams chat conversation, ask for a review and link the issue. Example:

    Bonjour, quelqu'un peut regarder SVP:
    https://gitlab.com/natural-solutions/ecoteka-landing-page/-/merge_requests/15

## 6/ If approved, merge the branch

Return to your merge request page on Gitlab, hit _squash commits_ and _delete source files_ and then click the _merge_ button.

## 7/ Run pipeline

On the left menu, go to _CI/CD_ and click the _Run pipeline_ button. If the pipeline passes, your work is done :)
