## Quick definitions

### Docker

Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. It is lighter than a virtual machine.

### Image

A snapshot of a part of an application. It containes files, libraries, dependencies. It can not be changed and is read-only. It is a template, a base to build a container.

### Container

An instance of an image. It depends on an image to exist. It is independant from the other containers of the application.

## Create an image

You need a Dockerfile in the repository.

Then, run:

    docker build

To tag the image:

    docker build -t <name>

## Run a container

    docker run -d <imagename>

## Manage containers

List all containers:

    docker ps -a

List all running containers:

    docker ps

Follow logs of a container:

    docker logs -f <containername>

## Write a Dockerfile

A script file with instructions to build a specific Docker image.

If you create the image yourself, it will always be based on another image. So the Dockerfile will contain, at the top:

    FROM <image_its_based_on>

To run a command while building the image:

    RUN <command>

To expose a port:

    EXPOSE <port>

## Run the app with Compose

_docker-compose.yml_ is a script file with instructions to orchestrate and automatise a Docker application. It will create the containers (named _services_), using the Dockerfiles of each image.

    docker-compose up -d

### Sources

[Docker official website](https://www.docker.com/)

[Docker CLI cheat sheet](https://dockerlabs.collabnix.com/docker/cheatsheet/)

[Docker Compose docs](https://docs.docker.com/compose/gettingstarted/)
