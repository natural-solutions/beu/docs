## Quick definition

Git is a version control or source control tool which tracks and manages the different versions and changes of a code. 

If a mistake is made, you can go back or compare versions.

You can use a graphic interface as GitHub Desktop or GitKraken, or simply learn the command lines.

## Quick workflow

1/ Clone repository, switch to a branch if necessary

2/ Pull changes so your local repository is up to date

3/ Add your changes for your new commit

4/ Commit your changes

5/ Push your changes


## Setup and branches

Transform the current directory in a Git repository:

    git init

Create an empty Git repo in a specified directory:

    git init <directory>


Clone a distant repository:

    git clone [url]

Switch to another branch:

    git checkout <branch>


## Stages and status

Stage all changes before a commit:

    git add .

Stage all changes in a specific directory:

    git add <directory>

List which files are staged, unstaged, and untracked:

    git status

List what is changed but not staged:

    git diff

## Integrating and retrieving changes

Commit your staged content to create a new snapshot:

    git commit -m “<descriptive message explaining the changes>”

Update the distant repository in GitLab:

    git push

Retrieve the changes in the remote repository:

    git pull


### Sources

[GitHub Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)

[Atlassian Git Cheat Sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)