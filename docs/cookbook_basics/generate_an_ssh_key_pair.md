
## Quick definition

The SSH protocol (keys) allows you to authenticate to GitLab or Github remote servers without supplying your username and password each time. 

The SSH agent will remember your passphrase and allows you to not enter it every time you use your key.

## Generate an SSH key

In a Git Bash terminal:

    ssh-keygen -t ed25519 -C "your_email@example.com"

You will be able to change the file name / location or to press enter to accept the default one : 
    
    > Enter a file in which to save the key (/c/Users/you/.ssh/id_algorithm):[Press enter]

You will be asked to enter a secure passphrase:

    > Enter passphrase (empty for no passphrase): [Type a passphrase]
    > Enter same passphrase again: [Type passphrase again]

## Add the key to the SSH agent

Start the ssh-agent:

    eval "$(ssh-agent -s)"
    > Agent pid 59566

Add the private key. If you changed the key name, replace *id_ed25519* in the command with the name of your private key file.
    
    ssh-add ~/.ssh/id_ed25519

Enter your passphrase.

    > Enter passphrase for /home/default/.ssh/id_ed25519:

## Add the public SSH key to your GitHub or GitLab account. 

Copy the public key.

In GitHub or GitLab, select *Preferences (or Settings) > SSH Keys*. Give the new key a title. In the input *Add a new SSH key*, paste the key. Example:

    SHA256:xcV6HDxyRb****/2cJ4L4****UK9ZBwdVyqm****n+g



### Sources

[GitLab SSH docs](https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair)

[GitHub SSH docs](https://docs.github.com/en/authentication/connecting-to-github-with-ssh)