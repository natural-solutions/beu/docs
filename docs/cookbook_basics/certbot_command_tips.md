## Getting certificates


### Obtain and install a certificate:

    certbot

### Obtain a certificate but don't install it:

    certbot certonly

### You may specify multiple domains with -d and obtain and install different certificates by running Certbot multiple times:
    
    certbot certonly -d example.com -d www.example.com
    certbot certonly -d app.example.com -d api.example.com


## Managing certificates

To view a list of the certificates Certbot knows about, run the certificates subcommand:

```
certbot certificates
```

This returns information in the following format:

```
Found the following certificates:
  Certificate Name: example.com
    Domains: example.com, www.example.com
    Expiry Date: 2017-02-19 19:53:00+00:00 (VALID: 30 days)
    Certificate Path: /etc/letsencrypt/live/example.com/fullchain.pem #path of your certificate
    Key Type: RSA
    Private Key Path: /etc/letsencrypt/live/example.com/privkey.pem
```

with the option <b>--cert-name</b>, you can specify the url of the certificate.
