## frontend / components

Contains all react components: buttons, forms, header etc

## frontend / pages

Contains pages: index, signin etc

Each page is a react function component that is built using the components of the _components_ folder.

## frontend / providers

Contains the _AppContext.tsx_ file. This file is used in the pages and components to check the app context. The app context contains global values to be shared in all the application (rather than in a specific component).
For instance:

The constant _user_'s value changes depending on the existence of a logged-in user.

    frontend / providers / AppContext.tsx
    ....
    const [user, setUser] = useLocalStorage<IUser>("user");

In the general page layout, we use this constant to display the Subfooter only when there is no logged in user.

    frontend / components / AppLayout / General.tsx
    .....
    import { useAppContext } from "@/providers/AppContext";
    ....
    const { user } = useAppContext();
    ...
     {!user && <Subfooter />}

## frontend / public / assets

Contains the pictures, icons, logos etc.

## frontend / public / locales

Contains the English, Spanish and French translations json files. Keys are referred in the components thanks to the [i18next](https://www.i18next.com/) translation framework. For example:

In each folder (en, fr, es), we define the _LayoutFooter.caption_ key in the components.json file:

    frontend / public / locales / en / components.json
    .....
    "LayoutFooter": {
        "caption": "get into this?"
    }
    ....

In the general page layout, we use the key, trusting i18next to display the right translation:

    frontend / components / AppLayout / General.tsx
    .....
    import { useTranslation } from "react-i18next";
    .....
    const LayoutSubFooter: FC = () => {
        const { t } = useTranslation();
        .....
        <Typography>
            {t("components.LayoutFooter.caption")}
        </Typography>
        ....
    };
    ....

## frontend / theme

Contains the _config.ts_ file. It defines the color values for the two themes (light and dark). For instance:

    frontend / theme / config.ts
    ....
    const themes = {
        light: {
            palette: {
            type: "light",
            background: {
                default: "#fbfdfe",
                paper: "#fff",
                dark: "#384145",
                shade: "#2D363A",
            },
        ....
        }
    ....
    }

The _makeStyles_ function allows you to use JS to style components. Here, the "content" object is the className (or id) that we will attribute to the <main> element.

    frontend / components / AppLayout / General.tsx
    ....
    import { makeStyles } from "@material-ui/core";
    ....
    const useStyles = makeStyles((theme) => ({
        content: {
            backgroundColor: theme.palette.background.default,
            minHeight: "calc(100vh - 48px)",
            paddingTop: theme.spacing(1),
            paddingBottom: theme.spacing(3),
        },
    }));
    ....
    const AppLayoutGeneral: FC<IAppLayoutGeneral> => {
        return (
        <main className={classes.content}>{children}</main>
      );
    };
