## Front End

### [Next.js](https://nextjs.org/)

Framework for React using a page-based routing system and fast refresh.

### [Material-UI](https://mui.com/)

User Interface (UI) library of components for React.

## Back End

### [FastApi](https://fastapi.tiangolo.com/)

Python framework to build APIs.

## Database

### [PostgreSQL 12](https://www.postgresql.org/docs/12/index.html)

Object-relational open-source database system.

### [SQLAlchemy](https://www.sqlalchemy.org/)

Python SQL Toolkit and Object Relational Mapper.

### [Alembic](https://alembic.sqlalchemy.org/en/latest/)

Database migration tool to use with SQLAlchemy.

## Maps

### [Deck.gl](https://deck.gl/docs/get-started/)

Open source data visualization framework made by Uber. Used to display maps, layers, data etc.

### [Tippecanoe](https://github.com/mapbox/tippecanoe)

Builds vector tilesets from geoJSON. Allows the view of the data to be independent from the scale of the mpa.

## Proxy

### [Traefik](https://doc.traefik.io/traefik/)

Open source Edge Router. Intercepts and routes the requests from the internet to the corresponding service.

